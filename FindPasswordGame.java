package gamePart;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class FindPasswordGame {

    private static final Scanner scanner = new Scanner(System.in);
    private static final ArrayList<String> enteredWord = new ArrayList<String>();

    public static void main(String[] args) {
        String secretWord = "muzammil";
        System.out.print("Enter your limit for finding password: ");
        int limit = scanner.nextInt();
        scanner.nextLine();
        try {
            boolean finding = findPassword(limit, secretWord);
            if (finding) {
                System.out.println("Congratulations! You found....");
            } else {
//                Iterator iterator = enteredWord.iterator();
//                while (iterator.hasNext()){
                for (String wrongWord : enteredWord) {
                    System.out.println("You entered <" + wrongWord + "> for finding password but it is incorrect....");
                }
                System.out.println("Sorry! You lose....");
            }
        } catch (InputMismatchException inputMismatchException) {
            System.out.println("You entered " + secretWord.length() + " characters but it is incorrect,\n" +
                    "You should at least 6 character or less than 13....");
        }
    }

    private static boolean findPassword(int limit, String secretWord) {
        int count = 0;
        String guessWord;
        while (true) {
            if (limit > count) {
                System.out.print("Enter your word for finding password (length 6-12): ");
                guessWord = scanner.nextLine();
                if (guessWord.length() > 5 && guessWord.length() <= 12) {
                    enteredWord.add(guessWord);
                    if (secretWord.equals(guessWord.toLowerCase())) {
                        return true;
                    } else {
                        System.out.println("Not found! Try again");
                    }
                    count++;
                } else {
                    throw new InputMismatchException("Error!");
                }
            } else {
                break;
            }
        }
        return false;
    }
}